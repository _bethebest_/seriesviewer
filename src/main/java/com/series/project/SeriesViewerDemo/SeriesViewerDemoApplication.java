package com.series.project.SeriesViewerDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeriesViewerDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeriesViewerDemoApplication.class, args);
	}
}
