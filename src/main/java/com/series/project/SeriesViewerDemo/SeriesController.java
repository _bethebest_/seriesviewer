package com.series.project.SeriesViewerDemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
public class SeriesController {
	@Autowired
	SeriesRepository seriesRepository;
/*	
	@RequestMapping(path="/series", method=RequestMethod.GET)
	public List showSeries()
	{
		return (List) seriesRepository.findAll();
	}*/
	@RequestMapping(path="/series")
	public List<SeriesDao> showSeries(@RequestParam(name = "page", defaultValue = "0") int pageNumber)
	{
		PageRequest request = new PageRequest(pageNumber, 25);
		return seriesRepository.findAll(request).getContent();
	}

	@RequestMapping(value="/showSeries")
	public ModelAndView model(){
		ModelAndView model = new ModelAndView("showSeries");
		return model;
	}
	

}
