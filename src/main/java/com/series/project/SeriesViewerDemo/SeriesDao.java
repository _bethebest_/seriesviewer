package com.series.project.SeriesViewerDemo;


import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="serie")
public class SeriesDao {
	@Id
	@Column(name="id")
	private long id;
	@Column(name="name")
	private String name;
	@Column(name="file")
	private String file;
	@Column(name="cassid")
	private UUID cassid;
	@Column(name="categoryid")
	private int categoryid;
	@Column(name="datefrom")
	@Temporal(TemporalType.DATE)
	private Date datefrom;
	@Column(name="dateto")
	@Temporal(TemporalType.DATE)
	private Date dateto;
	@Column(name="frequency")
	private int frequency;
	@Column(name="markers")
	private int markers[];
	@Column(name="unit")
	private String unit;
	@Column(name="feed")
	private String feed;
	@Column(name="createdate")
	private Date createdate;
	@Column(name="changedate")
	private Date changedate;
	
	
	public SeriesDao(){}
	
	
	public SeriesDao(String name, String file, UUID cassid, int categoryid, Date datefrom, Date dateto, int frequency,
			int[] markers, String unit, String feed, Date createdate, Date changedate) {
		super();
		this.name = name;
		this.file = file;
		this.cassid = cassid;
		this.categoryid = categoryid;
		this.datefrom = datefrom;
		this.dateto = dateto;
		this.frequency = frequency;
		this.markers = markers;
		this.unit = unit;
		this.feed = feed;
		this.createdate = createdate;
		this.changedate = changedate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public UUID getCassid() {
		return cassid;
	}
	public void setCassid(UUID cassid) {
		this.cassid = cassid;
	}
	public int getCategoryid() {
		return categoryid;
	}
	public void setCategoryid(int categoryid) {
		this.categoryid = categoryid;
	}
	public Date getDatefrom() {
		return datefrom;
	}
	public void setDatefrom(Date datefrom) {
		this.datefrom = datefrom;
	}
	public Date getDateto() {
		return dateto;
	}
	public void setDateto(Date dateto) {
		this.dateto = dateto;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public int[] getMarkers() {
		return markers;
	}
	public void setMarkers(int[] markers) {
		this.markers = markers;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getFeed() {
		return feed;
	}
	public void setFeed(String feed) {
		this.feed = feed;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public Date getChangedate() {
		return changedate;
	}
	public void setChangedate(Date changedate) {
		this.changedate = changedate;
	}
	@Override
	public String toString() {
		return "SeriesDao [id=" + id + ", name=" + name + ", file=" + file + ", cassid=" + cassid + ", categoryid="
				+ categoryid + ", datefrom=" + datefrom + ", dateto=" + dateto + ", frequency=" + frequency
				+ ", markers=" + Arrays.toString(markers) + ", unit=" + unit + ", feed=" + feed + ", createdate="
				+ createdate + ", changedate=" + changedate + "] \n ";
	}
	
	
}

