$(document).ready( function () {
	$('#dataTable').DataTable({
		"processing": true,
	    "serverSide": true,
		"ajax": {
            "url": "series",
            "dataSrc": ""
        },
			"columns": [
			    { "data": "id"},
			    { "data": "name" },
				{ "data": "file" },
				{ "data": "cassid" },
				{ "data": "categoryid" },
				{ "data": "datefrom" },
				{ "data": "dateto" },
				{ "data": "frequency" },
				{ "data": "markers" },
				{ "data": "unit" },
				{ "data": "feed" },
				{ "data": "createdate" },
				{ "data": "changedate" }
			]
	 })
});